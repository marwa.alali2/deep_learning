import streamlit as stl
from fastai.vision.all import load_learner, Path, torch, PILImage
from PIL import Image

import pathlib
temp = pathlib.PosixPath
pathlib.PosixPath = pathlib.WindowsPath


model = load_learner('export.pkl')

stl.title("Radiographie pulmonaire classifier ")
Image =  PILImage.create(r'val/NORMAL/NORMAL2-IM-1430-0001.jpeg')
stl.image(Image, caption=" C'est present un example de photo de radiographie pulmonaire ")

img_upload = stl.sidebar.file_uploader(
    "Sélectionner une image", type=['jpg', 'png','jpeg'])

if img_upload is not None:
    img_upload = PILImage.create(img_upload)
    stl.image(img_upload)
    if stl.button('Lancer analyse'):
        pred = model.predict(img_upload)[0]
        if pred == 'erreur':
            stl.warning("Est-ce un vraiment un photo NORMAL ?")
        else:
            stl.success(f"L'analyse conclu qu'il s'agit d'un {pred}   ")
